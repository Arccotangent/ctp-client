#include <iostream>
#include <cstring>
#include <cstdio>
#include <sstream>
#include <stdexcept>
#include <iomanip>
#include <zlib.h>
#include <fstream>
#include <ctime>
#include "base64.h"
#include <boost/asio.hpp>
using namespace std;
using namespace boost;
using namespace boost::asio::ip;
asio::io_service ioserv;
string msg;
string dmsg;
const size_t BUFFER_SIZE = 4096; //You can customize the buffer size in bytes with this!
clock_t ci, cf;
double sd, kbytes_ps;

/** Compress a STL string using zlib with given compression level and return
  * the binary data. */
std::string zcompress(const std::string& str,
							int compressionlevel = Z_BEST_COMPRESSION)
{
	z_stream zs;                        // z_stream is zlib's control structure
	memset(&zs, 0, sizeof(zs));

	if (deflateInit(&zs, compressionlevel) != Z_OK)
		throw(std::runtime_error("deflateInit failed while compressing."));

	zs.next_in = (Bytef*)str.data();
	zs.avail_in = str.size();           // set the z_stream's input

	int ret;
	char outbuffer[32768];
	std::string outstring;

	// retrieve the compressed bytes blockwise
	do {
		zs.next_out = reinterpret_cast<Bytef*>(outbuffer);
		zs.avail_out = sizeof(outbuffer);

		ret = deflate(&zs, Z_FINISH);

		if (outstring.size() < zs.total_out) {
			// append the block to the output string
			outstring.append(outbuffer,
							 zs.total_out - outstring.size());
		}
	} while (ret == Z_OK);

	deflateEnd(&zs);

	if (ret != Z_STREAM_END) {          // an error occurred that was not EOF
		std::ostringstream oss;
		oss << "Exception during zlib compression: (" << ret << ") " << zs.msg;
		throw(std::runtime_error(oss.str()));
	}
	return base64_encode(reinterpret_cast<const unsigned char*>(outstring.c_str()), outstring.length());
	//return outstring;
}

/** Decompress an STL string using zlib and return the original data. */
std::string zdecompress(const std::string& base64_str)
{
	const string str = base64_decode(base64_str);
	z_stream zs;                        // z_stream is zlib's control structure
	memset(&zs, 0, sizeof(zs));

	if (inflateInit(&zs) != Z_OK)
		throw(std::runtime_error("inflateInit failed while decompressing."));

	zs.next_in = (Bytef*)str.data();
	zs.avail_in = str.size();

	int ret;
	char outbuffer[32768];
	std::string outstring;

	// get the decompressed bytes blockwise using repeated calls to inflate
	do {
		zs.next_out = reinterpret_cast<Bytef*>(outbuffer);
		zs.avail_out = sizeof(outbuffer);

		ret = inflate(&zs, 0);

		if (outstring.size() < zs.total_out) {
			outstring.append(outbuffer,
							 zs.total_out - outstring.size());
		}

	} while (ret == Z_OK);

	inflateEnd(&zs);

	if (ret != Z_STREAM_END) {          // an error occurred that was not EOF
		std::ostringstream oss;
		oss << "Exception during zlib decompression: (" << ret << ") "
			<< zs.msg;
		cerr << oss.str() << endl;
		//throw(std::runtime_error(oss.str()));
	}

	return outstring;
}

void ctpconn(string address)
{
	string s3;
	try
	{
		tcp::resolver rslv(ioserv);
		tcp::resolver::query q(address, "15945");
		tcp::resolver::iterator endpoint_iter = rslv.resolve(q);
		tcp::socket sock(ioserv);
		cout << "Awaiting message from server. Client is configured to use a buffer size of " << BUFFER_SIZE << " bytes." << endl;
		asio::connect(sock, endpoint_iter);
		malloc(8192);
		ci = clock();
		int t = 0;
		for (;;)
		{
		  char buf[BUFFER_SIZE];
		  boost::system::error_code error;

		  sock.read_some(boost::asio::buffer(buf), error);

		  if (error == boost::asio::error::eof)
			break; // Connection closed cleanly by peer.
		  else if (error)
			throw boost::system::system_error(error); // Some other error.
		  s3 = s3 + buf;
		  t++;
		  if (t % 10 == 0)
			cout << "[CTP/SOCKET] Received " << s3.length() << " bytes in " << t << " buffers.\r" << flush;
		}
		cf = clock();
		cout << "[CTP/SOCKET] Received " << s3.length() << " bytes in " << t << " buffers.\r" << endl;
		clock_t cd = cf - ci;
		sd = (double)cd / (double)CLOCKS_PER_SEC;
		cout << "[CTP/SOCKET] Transmission complete in " << sd << " seconds!" << endl;
		kbytes_ps = s3.length() / sd;
		kbytes_ps /= 1024;
		cout << "[CTP/SOCKET] Average compressed transmission speed: " << kbytes_ps << " KB/s" << endl;
		msg = s3;
		s3 = "";
		sock.close();
	}
	catch (std::exception& e)
	{
		cerr << e.what() << endl;
	}
}

int main()
{
	fprintf(stdout, "CTP (Compressed Transmission Protocol) compresses data transfers using ZLIB and saves you bandwidth.\nHowever, it is in very early development.\n");
	string x;
	cout << "CTP Server Address: ";
	cin >> x;
	ctpconn(x);
	cout << "[CTP/ZIP] Decompressing data." << endl;
	dmsg = zdecompress(msg.c_str());
	size_t cms = msg.length();
	size_t dms = dmsg.length();
	cout << "[CTP/ZIP] Decompressed transmission size: " << dms << " bytes" << endl;
	kbytes_ps = (double)dms / sd;
	kbytes_ps /= 1024;
	cout << "[CTP/SOCKET] Average decompressed transmission speed: " << kbytes_ps << " KB/s (what would be if the decompressed information were transmitted in the same amount of time)" << endl;
	if (dms > 1024)
	{
		cout << "[CTP/WARN] WARNING: Decompressed transmission size is greater than 1 KB. Do you want to save output to a file instead? (y/n): ";
		char yn1;
		cin >> yn1;
		if (yn1 == 'Y' || yn1 == 'y')
		{
			cout << "File name: ";
			string fn;
			cin >> fn;
			fstream fns;
			fns.open(fn.c_str(), ios_base::out);
			fns.write(dmsg.c_str(), dms);
			fns.close();
			cout << "Written message to file." << endl;
		}
		else
		{
			cout << "-----BEGIN CTP TRANSMISSION-----" << endl;
			cout << "CMS: " << cms << endl;
			cout << "DMS: " << dms << endl;
			cout << endl;
			cout << dmsg << endl;
			cout << "-----END CTP TRANSMISSION-----" << endl;
		}
	}
	else
	{
		cout << "-----BEGIN CTP TRANSMISSION-----" << endl;
		cout << "CMS: " << cms << endl;
		cout << "DMS: " << dms << endl;
		cout << endl;
		cout << dmsg << endl;
		cout << "-----END CTP TRANSMISSION-----" << endl;
	}
	cout << "CTP client done! Exiting." << endl;
	return 0;
}

